package com.baeldung.spring.cloud.kubernetes.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
public class BackendController {

	@GetMapping("/first")
	List<String> getList() {
		System.out.println("In the backend controller - /first");
		List<String> theItems = new ArrayList()<String>;
		theItems.add("one");
		theItems.add("two");
		return theItems;
	}
	
}